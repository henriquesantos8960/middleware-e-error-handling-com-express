import express, {Express, Request, Response} from 'express';
import Connection from "./database/sequelize";
import bodyParser from "body-parser";

import films from "./api/controllers/FilmController";

const app: Express = express();
const port: number = 3333;

app.use(bodyParser.json());

app.use("/films", films);

app.get("/", (req: Request, res: Response) => {
    res.send("Hello World Express + Typescript");
});

app.listen(port, () => {
    console.log(`Este app está sendo ouvido na porta ${port}`);
});

Connection();